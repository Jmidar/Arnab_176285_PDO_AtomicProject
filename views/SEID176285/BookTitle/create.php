<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container" style="background-color: whitesmoke; margin-top: 30px">
    <h1 align="center">Book Title - Create Form</h1>
    <form action="store.php" method="post">

        <div class="form-group">
            <label for="BookTitle">Book Title</label>
            <input type="text" class="form-control" name="BookTitle" placeholder="Enter Book Title Here...">
        </div>
        <div class="form-group">
            <label for="AuthorName">Author Name</label>
            <input type="text" class="form-control" name="AuthorName" placeholder="Enter Author Name Here...">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

</body>
</html>