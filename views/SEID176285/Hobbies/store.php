<?php

require_once ("../../../vendor/autoload.php");
use App\Utility\Utility;
$obj = new App\Hobbies\Hobbies();
$str_hobbies = implode (",",$_POST["hobbies"]);
$_POST ["hobbies"]= $str_hobbies;
$obj->setData($_POST);
$obj->store();
Utility::redirect("create.php");