<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 9/20/2017
 * Time: 9:59 AM
 */

namespace App\Message;
if(!isset($_SESSION)) session_start();

class Message
{
    public  static  function  setMessage($msg){
        return $_SESSION['message'];
        
    }
    public  static  function  getMessage($msg){
        $tempMsg =  $_SESSION['message'];
        $_SESSION['message'] = "";
    }
    public  static  function  message($msg=NULL){
        if(is_null($msg))
            return self::getMessage();
        else
            return self::setMessage();
    }

}