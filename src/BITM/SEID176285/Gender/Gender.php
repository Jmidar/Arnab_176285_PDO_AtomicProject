<?php

namespace App\Gender;


use App\Model\Database;

class Gender extends Database
{
    public $id, $name, $gender;


    public function setData ($postArray){

        if(array_key_exists("id",$postArray))
            $this->id = $postArray['id'];

        if(array_key_exists("name",$postArray))
            $this->name = $postArray['name'];

        if(array_key_exists("gender",$postArray))
            $this->gender = $postArray['gender'];


    } //end of setData method
    public function store(){

        //$sqlQuery = "INSERT INTO book_title (book_title, author_name) VALUES (?,?)";

        $sqlQuery = "INSERT INTO gender (name, gender) VALUES ( ?,?)";

        $dataArray = [$this->name, $this->gender];

        $sth = $this->dbh->prepare($sqlQuery);

        $status = $sth->execute($dataArray);

        if($status){

            echo "Your data has been stored successfully<br>";
        }
        else
            echo "Failed! data has not been stored<br>";

    }
}