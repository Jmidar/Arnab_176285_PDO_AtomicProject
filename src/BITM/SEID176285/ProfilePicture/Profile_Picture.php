<?php


namespace App\ProfilePicture;


use App\Model\Database;

class Profile_Picture extends Database
{
    public $id, $name, $picture;


    public function setData ($postArray){

        if(array_key_exists("id",$postArray))
            $this->id = $postArray['id'];

        if(array_key_exists("name",$postArray))
            $this->name = $postArray['name'];

        if(array_key_exists("picture",$postArray))
            $this->picture = $postArray['picture'];


    } //end of setData method
    public function store(){

        //$sqlQuery = "INSERT INTO book_title (book_title, author_name) VALUES (?,?)";

        $sqlQuery = "INSERT INTO profile_picture (name, picture) VALUES ( ?,?)";

        $dataArray = [$this->name, $this->picture];

        $sth = $this->dbh->prepare($sqlQuery);

        $status = $sth->execute($dataArray);

        if($status){

            echo "Your photo has been stored successfully<br>";
        }
        else
            echo "Failed! photo email has not been stored<br>";


    }

}