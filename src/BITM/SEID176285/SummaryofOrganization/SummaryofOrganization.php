<?php
namespace App\SummaryofOrganization;



use App\Model\Database;

class SummaryofOrganization extends Database
{
    public $id, $organizationName, $summary;


    public function setData($postArray){

        if(array_key_exists("id",$postArray))
            $this->id = $postArray["id"];


        if(array_key_exists("Organization Name",$postArray))
            $this->organizationName = $postArray["OrganizationName"];


        if(array_key_exists("Summary",$postArray))
            $this->summary = $postArray["Summary"];

    }// end of setData Method


    public function store(){

        $sqlQuery = "INSERT INTO summary_of_orgamization (organization_Name, summary) VALUES (?, ?)";
        $sth = $this->dbh->prepare( $sqlQuery );


        $dataArray = [ $this->organizationName, $this->summary ];

        $status = $sth->execute($dataArray);

        if($status)
            echo "Success! Data has been inserted successfully<br>";
        else
            echo "Failed! Data has not been inserted<br>";

    }// end of store() Method



}// end of BookTitle Class