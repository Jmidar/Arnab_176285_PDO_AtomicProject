<?php

namespace App\City;


use App\Model\Database;

class City extends Database
{
    public $id, $city;

    public function setData($postArray){

        if(array_key_exists("id",$postArray))
            $this->id = $postArray['id'];

        if(array_key_exists("city",$postArray))
            $this->city = $postArray['city'];

    } //end of setData method
    public function store(){

        //$sqlQuery = "INSERT INTO book_title (book_title, author_name) VALUES (?,?)";

        $sqlQuery = "INSERT INTO city (city) VALUES (?)";

        $dataArray = [$this->city];

        $sth = $this->dbh->prepare($sqlQuery);

        $status = $sth->execute($dataArray);

        if($status){

            echo "Your City Name has been stored successfully<br>";
        }
        else
            echo "Failed! your City name has not been stored<br>";


    }




    }
