<?php


namespace App\Email;


use App\Model\Database;

class Email extends Database
{
     public $id, $name, $email;


    public function setData ($postArray){

        if(array_key_exists("id",$postArray))
            $this->id = $postArray['id'];

        if(array_key_exists("name",$postArray))
            $this->name = $postArray['name'];

        if(array_key_exists("email",$postArray))
            $this->email = $postArray['email'];


    } //end of setData method
    public function store(){

        //$sqlQuery = "INSERT INTO book_title (book_title, author_name) VALUES (?,?)";

        $sqlQuery = "INSERT INTO email (name, email) VALUES ( ?,?)";

        $dataArray = [$this->name, $this->email];

        $sth = $this->dbh->prepare($sqlQuery);

        $status = $sth->execute($dataArray);

        if($status){

            echo "Your email has been stored successfully<br>";
        }
        else
            echo "Failed! your email has not been stored<br>";


    }

}
