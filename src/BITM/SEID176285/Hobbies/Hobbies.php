<?php

namespace App\Hobbies;

use App\Model\Database;

class Hobbies extends Database
{
    public $id, $name, $hobbies;


    public function setData ($postArray){

        if(array_key_exists("id",$postArray))
            $this->id = $postArray['id'];

        if(array_key_exists("name",$postArray))
            $this->name = $postArray['name'];

        if(array_key_exists("hobbies",$postArray))
            $this->hobbies = $postArray['hobbies'];

    } //end of setData method

    public function store(){


        //$sqlQuery = "INSERT INTO book_title (book_title, author_name) VALUES (?,?)";

        $sqlQuery = "INSERT INTO hobbies (name, hobbies) VALUES ( ?,?)";

        $dataArray = [$this->name, $this->hobbies];

        $sth = $this->dbh->prepare($sqlQuery);

        $status = $sth->execute($dataArray);

        if($status){

            echo "Your data has been stored successfully<br>";
        }
        else
            echo "Failed! data has not been stored<br>";

    }
}